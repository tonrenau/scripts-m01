# @edt ASIX M01-ISO
## Curs 2020-2021

Podeu trobar els programes al GitLab de [edtasixm01](https://gitlab.com/edtasixm01/scripts-2019.git
)

Podeu trobar la documentació del mòdul a [ASIX-M01](https://sites.google.com/site/asixm01edt/)

ASIX M06-ASO Escola del treball de barcelona


Llistat d'exercicis i material

 * 1 - exercicis fets a classe (19 exercicis)

 * 2 - exercicis_scripts_basics (10 exercicis)

 * 3 - exercicis_scripts (12 exercicis)

