#! /bin/bash
# @edt ASIX-M01 Curs 2017-2018
# gener 2018
# Descripcio: copiar files a dir desti
# synopsis: $ prog file... dirDesti
# ---------------------------------
ERR_DIR=2
ERR_NARGS=1
#1) Validar num args
if [ $# -lt 2 ]
then
  echo "Error num args"
  echo "usage: $prog file... dirDesti"
  exit $ERR_NARGS
fi
dirDesti=$(echo $*| cut -d' ' -f$#)
llistaFiles=$(echo $* | cut -d' ' -f1-$(($#-1)))
#2) Validar es un dir (si no err)
if [ ! -d "$dirDesti" ]
then
  echo "Error arg is not dir"
  echo "usage: $ prog file... dirDesti"
  exit $ERR_DIR
fi

#3) Iterar file a file
for file in $llistaFiles
do
  if [ ! -f "$file" ]
  then
    echo "Error: file $file is not a regular file" >> /dev/stderr
  else
    echo "cp $file $dirDesti"
  fi 
done



