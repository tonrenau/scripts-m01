#! /bin/bash
# @edt ASIX-M01 Curs 2017-2018
# gener 2018
# Descripcio: exemple validar args amb if
# synopsis: $ prog -h / -a arg / -b -a arg
# -----------------------------------------
OK=0
ERR_DIR=2
ERR_NARGS=1
ERR_ARGS=2
#1) validar si help
if [ $# -eq 1 -a "$1" = "-h" ]
then
  echo "@edt ASIX-M01 Curs 2017-2018"
  echo "usage: $prog -h / -a arg / -b -a arg"
  exit $OK
fi
#2) Validar num args
if [ $# -lt 2  -o $# -gt 3 ]
then
  echo "Error num args"
  echo "usage: $prog -h / -a arg / -b -a arg"
  exit $ERR_NARGS
fi
# Primer cas
if [ $# -eq 2 -a "$1" != "-a" ]
then
  echo "Errorformat args"
  echo "usage: $prog -h / -a arg / -b -a arg"
  exit $ERR_ARGS
fi
# Segon cas
if [ $# -eq 3 -a "$1" != "-b" -a "$1" != "-a" ]
then
  echo "Errorformat args"
  echo "usage: $prog -h / -a arg / -b -a arg"
  exit $ERR_ARGS
fi
#### xixa ####
if [ $# -eq 2 ]
then
  echo "Primer cas, arg val $1"
else
  echo "Segon cas, arg val $2"
fi
exit $Ok
