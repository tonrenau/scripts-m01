#!/bin/bash
# @edt ASIX-M01 2017-2018
# Gener 2018
# Exemples de if
# -------------------------------------
num=$1
if [ $num -ge 0 -a $num -lt 18  -o $num -ge 65 ]
then
  echo "ok"
fi
exit 0

num=$1
if [ $num -ge 0 -o $num -le 20 ]
then
  echo "ok"
fi
exit 0

nom1="pere"
nom2=""
if [ -n "$nom1" ]
then
 echo "$nom1 no esta buit"
fi

if [ -n "$nom2" ]
then
 echo "$nom2 no esta buit"
fi
exit 0

nom1="pere"
nom2=""
if [ -z "$nom1" ]
then
 echo "$nom1 esta buit"
fi

if [ -z "$nom2" ]
then
 echo "$nom2 esta buit"
fi
exit 0

num=$1
if ! [ $num -gt 10 ]
then
  echo "petit o igual"
fi

if [ "mama" = "papa" ]
then
  echo "igual"
fi
exit 0

num=$1

if [ "$num" -eq 5 ]
then
  echo "son iguals"
fi

if [ "$num" -gt 5 ]
then
  echo "es mes gran"
fi


