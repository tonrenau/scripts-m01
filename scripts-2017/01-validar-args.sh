#!/bin/bash
# @edt ASIX-M01 2017-2018
# Gener 2018
# Validar que s'han rebut dos args
# Synpsis: $ prog.sh arg1 arg2
# -------------------------------------

ERR_NARG=1
OK=0
# 1) Validar args
if [ $# -ne 2 ]
then
  echo "ERROR numero args"
  echo "usage: prog.sh arg1 arg2"
  exit $ERR_NARG
fi
# 2) Fer xixa
echo "Primer: $1"
echo "Segon: $2"
exit $OK
