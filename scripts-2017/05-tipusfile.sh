#! /bin/bash
# @edt  ASIX-M01 Curs 2017-2018
# gener 2017
# Descripcio: Validar arg, dir si regular, link, dir
# synopsis: $ prog.sh file
# ---------------------------------
ERR_NARGS=1
OK=0
#1) Validar Que n'hi ha un arg
if [ $# -ne 1 ]
then
  echo "Error, numero d'arguments no valid"
  echo "Usage: prog.sh file"
  exit $ERR_NARGS
fi

fit=$1
if [ -d "$fit" ]
then
  msg="es un directori"
elif [ -h "$fit" ]
then
  msg="es un symbolic link"
elif [ -f "$fit" ]
then
  msg="es un regular file"
elif [ ! -e "$fit" ]
then
  msg="no existeix"
else
  msg="es una altra cosa"
fi
echo "$fit: $msg"
exit $OK




