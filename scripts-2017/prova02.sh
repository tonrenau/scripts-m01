#!/bin/bash
# @edt ASIX-M01 2017-2018
# Gener 2018
# Exemple processar arguments
#
if [ $# -lt 5 ]
then
  echo "Error: numero args no valid"
  echo "Calen almenys 5 args"
  echo "usage:prog a1 a2 a3 a4 a5[...]"
  exit 1
fi


echo 'args $*: '   $*
echo 'args $@: '   $@
echo 'numargs $#'  $#
echo 'nomprog: $0' $0
echo 'primer: $1'  $1
echo $2
echo "..."
echo $9
echo ${10}
echo ${11}
echo ${12}


exit 0

