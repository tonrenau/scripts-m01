#! /bin/bash
# data: 05-02-2018
# descripcio:
# Programa:
#   a) Validar existeix almenys un file. Per a cada file comprimir-lo. Generar per stdout el nom del file comprimit si s̉ha comprimit correctament, o un missatge d̉error per stderror si no s̉ha pogut comprimir. En
#      finalitzar es mostra per stdout quants files ha comprimit.
#      Retorna status 0 ok, 1 error nº args, 2 si algun error en comprimir.
#   b) Ampliar amb el cas: prog -h|--help.
# synopsis:
# 	 prog.sh  file…
###################################################################################
ERR_ARG=1
status=0
comptador=0

if [ $# -eq 1 -a "$1" = "-h" ] 
then
  echo " help roberto@edt"
  echo " prog.sh que comprimeix tots els files "
  echo " usage: prog.sh arg..."
  exit $status
fi

# validar que hi ha un minim de 1

if [ $# -lt 1 ] 
then
  echo "error: n args no valid"
  echo "usage: prog.sh file"
  exit $ERR_ARG
fi

llista_files=$*
for file in $llista_files
do
  gzip $file &> /dev/null
  if [ $? -ne 0 ] 
  then
     echo "error: file: $file no se ha comprimit" >> /dev/stderr
     status=2
  else
    echo "$file: se ha comprimit" >> /dev/stdout
    comptador=$((comptador+1))
  fi
done
echo "files comprimits: $comptador"

exit $status
