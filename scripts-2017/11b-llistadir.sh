#! /bin/bash
# @edt ASIX-M01 Curs 2017-2018
# gener 2018
# Descripcio: llistar un dir, numera out 
# synopsis: $ prog dir
# ---------------------------------
ERR_DIR=2
ERR_NARGS=1
#1) Validar num args
if [ $# -ne 1 ]
then
  echo "Error num args"
  echo "usage: $prog dir"
  exit $ERR_NARGS
fi
#2) Validar es un dir (si no err)
dir=$1
if [ ! -d "$dir" ]
then
  echo "Error arg is not dir"
  echo "usage: $ prog dir"
  exit $ERR_DIR
fi
#3) Accio
llista_noms=$(ls $dir)
num=1
echo $llista_noms
for elem in $llista_noms
do
  echo "$num: $elem"
  num=$((num+1))
done








