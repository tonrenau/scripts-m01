#! /bin/bash
# @edt  ASIX-M01 Curs 2017-2018
# gener 2017
# Descripcio: Validar arg i dir si és un dir o no
# synopsis: $ prog.sh dir
# ---------------------------------
ERR_NARGS=1
OK=0
#1) Validar Que n'hi ha un arg
if [ $# -ne 1 ]
then
  echo "Error, numero d'arguments no valid"
  echo "Usage: prog.sh dir"
  exit $ERR_NARGS
fi
dir=$1
if [ -d "$dir" ]
then
  echo "$dir: es un directori"
else
  echo "$dir: no es un directori"
fi
exit $OK




