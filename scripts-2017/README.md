# ASIX-M01 UF2 Scripts Base
## @edt ASIX-M01 Curs 2017-2018

### Exercicis d'Scripts en Bash

 * Processar arguments
 * Processar l'entrada estàndard
 * Validar arguments
 * Seqüencial
 * Condicional: if/elif/else, case
 * Iterativa: for, while
 * Comptadors, acumuladors, concatenar
  
