#! /bin/bash
# @edt ASIX-M01 Curs 2017-2018
# gener 2018
# Descripcio: llistar tipus de elements de cada dir
# synopsis: $ prog dir...
# ---------------------------------
ERR_DIR=2
ERR_NARGS=1
#1) Validar num args
if [ $# -eq 0 ]
then
  echo "Error num args"
  echo "usage: $prog dir..."
  exit $ERR_NARGS
fi

for dir in $*
do
  if [ ! -d "$dir" ]
  then
    echo "Error $dir is not dir" >> /dev/stderr
  else
    echo -e "-------------------\nLlistat de $dir:\n---------------------"
    llista_noms=$(ls $dir)
    for nom in $llista_noms
    do
      fit=$dir/$nom
      if [ -d "$fit" ]
      then
        msg="es un directori"
      elif [ -h "$fit" ]
      then
        msg="es un symbolic link"
      elif [ -f "$fit" ]
      then
        msg="es un regular file"
      elif [ ! -e "$fit" ]
      then
        msg="no existeix"
      else
        msg="es una altra cosa"
      fi
      echo "$fit: $msg"
    done
  fi
done






