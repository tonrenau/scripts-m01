#! /bin/bash
# @edt ASIX M01 2017-18
# Programa que recoge los parametros y argumentos que se le suministran y los almacena en variables segun su tipo 
# synopsis: prog [-r -m -c apellido -j -e edad] arg...
#----------------------------
ERR_ARGS=1
if [ $# -lt 1 ]
then
  echo "ERROR, debe suministrar al menos un parametro"
  echo "usage: prog [-r -m -c apellido -j -e edad] arg..."
  exit $ERR_ARGS
fi

opc=""
arg=""
ape=""
edad=""

while [ -n "$1" ]
do
  case $1 in
    -[rmj]) 
	   opc="$opc $1";;
    -c)
       ape=$2
	   shift;;
    -e)
       edad=$2
	   shift;;
    *)
       arg="$arg $1";;
  esac
  shift
done
echo "opcions: $opc, cognom: $ape, edat: $edad, args: $args"
