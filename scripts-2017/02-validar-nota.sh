#!/bin/bash
# @edt ASIX-M01 2017-2018
# Gener 2018
# Validar nota i dir si aprovat/suspes
# Synpsis: $ prog.sh nota
# -------------------------------------

ERR_NARG=1
ERR_NOTA=2
OK=0
# 1) Validar args
if [ $# -ne 1 ]
then
  echo "ERROR numero args"
  echo "usage: prog.sh nota"
  exit $ERR_NARG
fi
# 2) Validar nota
nota=$1
if [ "$nota" -lt 0 -o "$nota" -gt 10  ]
then
  echo "ERROR valor nota"
  echo "usage: nota (0,10)"
  exit $ERR_NOTA
fi
# 3) xixa
if [ "$nota" -lt 5 ]
then
  echo "Suspes"
  exit 0
elif [ "$nota -ge 5 ]
then
  echo "Aprovat"
  exit 0
fi
exit $OK






