#!/bin/bash
# 05/02/2018
# ASIX 1
#
# Descripció:
#       Valida numero d'arguments i depenent del tipus demanat
#       i tots els arguments compleixen dit tipus
# Synopsis:
#       $ 07b_valida_arg.sh -f|-d arg1 arg2 arg3 arg4
#	$ 07b_valida_arg.sh -h|--help
#---------------------------------------------------------------------
ERR_NARG=1
status=0
#Help
if [ $# -eq 1 -a \("$1" = "-h" -o "$1" = "--help" \) ]
then
  echo "  Descripció:"
  echo "    Valida numero d'arguments i depenent del tipus demanat"
  echo "    i tots els arguments compleixen dit tipus"
  echo "  Synopsis:"
  echo "    $ 07b_valida_arg.sh -f|-d arg1 arg2 arg3 arg4"
  echo "  Creat per Samuel"
  exit $status
fi

#Validar args
if [ "$#" -ne 5 ]
then
  echo "ERROR numero args"
  echo "usage: 07_valida_arg.sh -f|-d arg1 arg2 arg3 arg4"
  exit $ERR_NARG
fi
if ! [ "$1" = "-f" -o "$1" = "-d" ]
then
  echo "ERROR format arguments"
  echo "usage: 07_valida_arg.sh -f|-d arg1 arg2 arg3 arg4"
  exit $ERR_NARG
fi

tipus="$1"
shift
#Comprovar tipus
for arg in $*
do
  if ! [ "$tipus" "$arg" ]
  then
    status=2
  fi
done
exit $status
