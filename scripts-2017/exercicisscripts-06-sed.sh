#!/bin/bash
#@edt ASIX-M01 2017-2018
#02/02/2018
#Descripcio: Tom Snyder --> T.Snyder
#Synopsis: prog stdin
#####################################################
OK=0
#llegim stdin
while read -r nom
do
  echo $nom | sed -r 's/^(.)[^ ]*/\1./'
done
exit $OK
