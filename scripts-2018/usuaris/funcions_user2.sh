#!/bin/bash

function showShadow(){
 ERR_NARGS=1
 ERR_NOLOGIN=2
 OK=0
 #1) validar arg
 if [ $# -ne 1 ]; then
   echo "Error: num args incorrecte"
   echo "usage: $0 login"
   return $ERR_NARGS
 fi
 #2) validar existeix
 login=$1
 line=""
 line=$(grep "^$login:" /etc/shadow 2> /dev/null)
 if [ -z "$line" ]; then
   echo "Error: login $login inexist"
   echo "usage: $0 login"
   return $ERR_NOLOGIN
 fi
 #3) mostrar
 passwd=$(echo $line | cut -d: -f2)
 last=$(echo $line | cut -d: -f3)
 min=$(echo $line | cut -d: -f4)
 max=$(echo $line | cut -d: -f5)
 warning=$(echo $line | cut -d: -f6)
 inactivity=$(echo $line | cut -d: -f7)
 expires=$(echo $line | cut -d: -f8)
 echo "login: $login"
 echo "passwd: $passwd"
 echo "last: $last"
 echo "min: $min"
 echo "max: $max"
 echo "warning: $warning"
 echo "inactivity: $inactivity"
 echo "expires: $expires"
 return 0
}



#showAllGroups
function showAllGroups(){
  llista_gids=$(cut -d: -f4 /etc/passwd \
    | sort -n | uniq)
  for gid in $llista_gids
  do
    num=$(grep "^[^:]*:[^:]*:[^:]*:$gid:" \
	    /etc/passwd | wc -l)
    if [ $num -ge 2 ]; then
      gname=$(grep "^[^:]*:[^:]*:$gid:" \
	      /etc/group | cut -d:1 -f1)
      echo $gid($gname)
      grep "^[^:]*:[^:]*:[^:]*:$gid:" \
	 /etc/passwd | cut -d: -f1,3,7 \
	 | sort | sed -r 's/^(.*)$/\t\1/' 
   fi
  done
}


function showAllShells(){
  llista_shells=$(cut -d: -f7 /etc/passwd \
	          | sort | uniq )
  for shell in $llista_shells 
  do
    num=$(grep ":$shell$" /etc/passwd \
	  | wc -l)
    if [ $num -ge 2 ]; then
      echo $shell: $num
      grep ":$shell$" /etc/passwd \
        | cut -d: -f1,3,4 \
        | sort -t: -k2g,2 \
        | sed -r 's/^(.*)$/\t\1/'     
    fi
  done  
}



function showGroupMainMembers(){
  ERR_NARGS=1
  ERR_NOGNAME=2
  Ok=0
  #1) validar arg
  if [ $# -ne 1 ]; then
    echo "Error: num args incorrecte"
    echo "usage: $0 gname"
    return $ERR_NARGS
  fi
 #2) validar existeix
 gname=$1
 gid=""
 gid=$(grep "^$gname:" /etc/group | cut -d: -f3)
 if [ -z "$gid" ]; then
   echo "Error: login $gname inexist"
   echo "usage: $0 gname"
   return $ERR_NOGNAME
 fi
 grep "^[^:]*:[^:]*:[^:]*:$gid:" /etc/passwd \
   | cut -d: -f1,3,7 | sort -t: -k2n,2 \
   | sed 's/:/  /g' | tr '[:lower:]' '[:upper:]'
 return $Ok
}



function showUserIn(){
  status=0
  while read -r login
  do
    line=$(grep "^$login:" /etc/passwd)
    if [ $? -ne 0 ]; then
      echo "Err: login $login inei" >> /dev/stderr
      status=$((status+1))
    else
      uid=$(echo $line | cut -d: -f3)
      gid=$(echo $line | cut -d: -f4)
      gname=$(grep "^[^:]*:[^:]*:$gid:" /etc/group | cut -d: -f1)
      gecos=$(echo $line | cut -d: -f5)
      home=$(echo $line | cut -d: -f6)
      shell=$(echo $line | cut -d: -f7)
      echo "$login $uid $gname($gid) $home $shell"
    fi
  done
  return $status
}


function showUserList(){
 ERR_NARGS=1
 status=0
 #1) validar arg
 if [ $# -lt 1 ]; then
   echo "Error: num args incorrecte"
   echo "usage: $0 login[...]"
   return $ERR_NARGS
 fi
 login_list=$*
 for login in $login_list
 do
  line=$(grep "^$login:" /etc/passwd)
  if [ $? -ne 0 ]; then
   echo "Err: login $login inei" >> /dev/stderr
   status=$((status+1))
  else
   uid=$(echo $line | cut -d: -f3)
   gid=$(echo $line | cut -d: -f4)
   gname=$(grep "^[^:]*:[^:]*:$gid:" \ 
         /etc/group | cut -d: -f1)
   gecos=$(echo $line | cut -d: -f5)
   home=$(echo $line | cut -d: -f6)
   shell=$(echo $line | cut -d: -f7)
   echo "login: $login"
   echo "uid: $uid"
   echo "$gname($gid)"
   echo "home: $home"
   echo "shell: $shell"
  fi
 done
 return $status
}

function showUser(){
 ERR_NARGS=1
 ERR_NOLOGIN=2
 OK=0
 #1) validar arg
 if [ $# -ne 1 ]; then
   echo "Error: num args incorrecte"
   echo "usage: $0 login"
   return $ERR_NARGS
 fi
 #2) validar existeix
 login=$1
 line=""
 line=$(grep "^$login:" /etc/passwd)
 if [ -z "$line" ]; then
   echo "Error: login $login inexist"
   echo "usage: $0 login"
   return $ERR_NOLOGIN
 fi
 #3) mostrar
 uid=$(echo $line | cut -d: -f3)
 gid=$(echo $line | cut -d: -f4)
 gname=$(grep "^[^:]*:[^:]*:$gid:" \ 
	 /etc/group | cut -d: -f1)
 gecos=$(echo $line | cut -d: -f5)
 home=$(echo $line | cut -d: -f6)
 shell=$(echo $line | cut -d: -f7)
 echo "login: $login"
 echo "uid: $uid"
 echo "$gname($gid)"
 echo "home: $home"
 echo "shell: $shell"
 return 0
}

