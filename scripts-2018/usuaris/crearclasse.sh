#! /bin/bash
# @edt ASIX M01-ISO
# crearclasse classe
# -------------------------------
status=0
BASEHOME=/home
#1) validar arg
ERR_NARGS=1
if [ $# -ne 1 ]; then
  echo "Err nargs incorrecte"
  echo "usage: $0 classe"
  exit $ERR_NARGS
fi
#2) existeix el grup/ crear-lo
ERR_CLASSEEXIST=2
classe=$1
grep "^$classe:" /etc/group &> /dev/null
if [ $? -eq 0 ]; then
  echo "Err group $classe exist"
  exit $ERR_CLASSEEXIST
fi
groupadd $classe && echo "Group $classe crated OK"
#3) crear directori base
ERR_DIREXIST=3
mkdir $BASEHOME/$classe &> /dev/null \
     && echo "mkdir done Ok"
if [ $? -ne 0 ]; then
  echo "Err dir /home/$classe ja exist"
  exit $ERR_DIREXIST
fi
chgrp $classe $BASEHOME/$classe && echo "chgrp OK"
#4) crear els usuaris
llista_users=$(echo $classe-{1,2})
for user in $llista_users
do
  useradd -m -n -g $classe -b $BASEHOME/$classe $user &> /dev/null  && echo "user $user created Ok"
  if [ $? -ne 0 ]; then
    echo "Err $user exist" >> /dev/stderr
    status=4
  fi
done
exit $status



