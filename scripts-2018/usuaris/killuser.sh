#! /bin/bash
# @edt ASIX M01-ISO Curs 2018-2019
#
# Eliminar un usuari
# #####################################
#1) validar arg
ERR_NARGS=1
if [ $# -ne 1 ]; then
  echo "Err nargs incorrecte"
  echo "usage: $0 login"
  exit $ERR_NARGS
fi
#2) existeix login?
ERR_NOLOGIN=2
login=$1
dataUser=""
dataUser=$(grep "^$login:" /etc/passwd 2> /dev/null)
if [ -z "$dataUser" ]; then
  echo "Err login $login inexistent"
  exit $ERR_NOLOGIN
fi

#3) eliminar compte usuari
echo userdel $login

#4) fer targz del home
#5) posar-hi el mbox
homeDir=$(echo $dataUser | cut -d: -f6)
tar cvzf /tmp/$login-home.tgz $homeDir /var/spool/mail/$login
echo rm -rf $homeDir
echo rm /var/spool/mail/$login

#6) llistar els files del sistema del user
echo find / -user $login -print 2> /dev/null

#7) llistar els processos de l'usuari
ps -u $login

#8) llistar les tasques d'impressió
lpq -U $login
lprm -U $login -

#9) at cron
atq -q $login
crontab -u $login -l
crontab -u $login -r




