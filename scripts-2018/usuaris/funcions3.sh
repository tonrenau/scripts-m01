#! /bin/bash
# @edt ASIX M01-ISO Curs 2018-2019

function getHome(){
  status=0
  home=""
  grep "^$1:" /etc/passwd | \
       cut -d: -f6 2> /dev/null
  return $?
}

getHomesArgs(){
  for login in $*
  do
    getHome $login 
  done	  
}

getSize(){
  du -sk $1 | cut -f1
  return $?
}

getSizeIN(){
  while read -r line
  do
    homeDir=$(getHome $line)
    getSize $homeDir    
  done	  
}

getAllSizes(){
  while read -r line
  do
    login=$(echo line | cut -d: -f1)
    homeDir=$(getHome $login)
    getSize $homeDir
  done < /etc/passwd
}

getAllSizes(){
  while read -r line
  do
    login=$(echo line | cut -d: -f1)
    homeDir=$(getHome $login)
    getSize $homeDir
  done < /etc/passwd
}

