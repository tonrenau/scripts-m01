#!/bin/bash
# @edt ASIX M01-ASO
# funcions de usuaris
# ----------

#showGroup(gname)
function showGroup(){
ERR_NARGS=1
 ERR_NOGNAME=2
 OK=0
 #1) validar arg
 if [ $# -ne 1 ]; then
   echo "Error: num args incorrecte"
   echo "usage: $0 gname"
   return $ERR_NARGS
 fi
 #2) validar existeix
 gname=$1
 line=""
 line=$(grep "^$gname:" /etc/group 2> /dev/null)
 if [ -z "$line" ]; then
   echo "Error: gname $gname inexist"
   echo "usage: $0 gname"
   return $ERR_NOGNAME
 fi
 #3) mostrar
 gid=$(echo $line | cut -d: -f3)
 userList=$(echo $line | cut -d: -f4)
 echo "gname: $gname"
 echo "gid: $gid"
 echo "userList: $userList"
 return 0
}


#showUserGecos(login)
function showUserGecos(){
 ERR_NARGS=1
 ERR_NOLOGIN=2
 OK=0
 #1) validar arg
 if [ $# -ne 1 ]; then
   echo "Error: num args incorrecte"
   echo "usage: $0 login"
   return $ERR_NARGS
 fi
 #2) validar existeix
 login=$1
 line=""
 line=$(grep "^$login:" /etc/passwd 2> /dev/null)
 if [ -z "$line" ]; then
   echo "Error: login $login inexist"
   echo "usage: $0 login"
   return $ERR_NOLOGIN
 fi
 #3) mostrar
 gecos=$(echo $line | cut -d: -f5)
 name=$(echo $gecos | cut -d, -f1)
 office=$(echo $gecos | cut -d, -f2)
 phoneOffi=$(echo $gecos | cut -d, -f3)
 phoneHome=$(echo $gecos | cut -d, -f4)
 echo "name: $name"
 echo "office: $office"
 echo "office phone: $phoneOffi"
 echo "home phone: $phoneHome"
 return 0
}

# showuser(login)
# mostrar un a un els camps amb label
function showUser(){
 ERR_NARGS=1
 ERR_NOLOGIN=2
 OK=0
 #1) validar arg
 if [ $# -ne 1 ]; then
   echo "Error: num args incorrecte"
   echo "usage: $0 login"
   return $ERR_NARGS
 fi
 #2) validar existeix
 login=$1
 line=""
 line=$(grep "^$login:" /etc/passwd 2> /dev/null)
 if [ -z "$line" ]; then
   echo "Error: login $login inexist"
   echo "usage: $0 login"
   return $ERR_NOLOGIN
 fi
 #3) mostrar
 uid=$(echo $line | cut -d: -f3)
 gid=$(echo $line | cut -d: -f4)
 gecos=$(echo $line | cut -d: -f5)
 home=$(echo $line | cut -d: -f6)
 shell=$(echo $line | cut -d: -f7)
 echo "login: $login"
 echo "uid: $uid"
 echo "gid: $gid"
 echo "home: $home"
 echo "shell: $shell"
 return 0
}

# funcio que saluda
function hola(){
  echo "hola bon dia"
  return 0
}

# suma els dos arguments que rep
function suma(){
  echo $(($1+$2))
  return 0
}


