#!/ bin/bash
# @edt ASIX M01-ISO Curs 2018-2019
#
# filtrar grups (només si usuaris principal)
# ------------------------------------------------

fileIn=/dev/stdin
if [ $# -eq 1 ]; then
  fileIn=$1
fi
while read -r line
do
  gid=$(echo $line | cut -d: -f3)
  num=$(grep "^[^:]*:[^:]*:[^:]*:$gid:" \
	  /etc/passwd 2> /dev/null | wc -l)
  if [ $num -ge 3 ]; then
    echo $line
    grep "^[^:]*:[^:]*:[^:]*:$gid:" \
      /etc/passwd | sort | cut -d: -f1,3 |  \
      sed -r 's/^(.*)$/\t\1/' 
  fi  
done < $fileIn
exit 0

fileIn=/dev/stdin
if [ $# -eq 1 ]; then
  fileIn=$1
fi
while read -r line
do
  gid=$(echo $line | cut -d: -f3)
  grep "^[^:]*:[^:]*:[^:]*:$gid:" \
      /etc/passwd &> /dev/null
  if [ $? -eq 0 ]; then
    echo $line
  fi
done < $fileIn
exit 0

