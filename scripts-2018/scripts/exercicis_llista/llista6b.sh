#! /bin/bash
# @edt ASIX M01 CURS 2018-2019
# Jordi Quirós Berbel
# Exercici 6 de la llista d'exercicis de bash ExercicisScripts.txt
# 19/02/2019
# ------------------------------------------------------------------------------

while read -r line
do
  echo $line | sed -r 's/^(.)[^ ]* /\1. /'
done

exit 0
