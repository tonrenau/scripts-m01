#! /bin/bash
# @edt ASIX-M01 Curs 2018-2019
# exercici 9
######################################
cognom=""
edat=""
opcions=""
arguments=""
while [ -n $1 ]
do
	case $1 in
	-r|-m|-j)
		opcions="$opcions $1";;
	-c)
		cognom=$2
		shift;;
	-e)
		edat=$2
		shift;;	
	*)
		arguments="$arguments $1";;
	esac
	shift
done
echo "El cognom: $cognom"
echo "El edat: $edat"
echo "Les opcions: $opcions"
echo "Els arguments: $arguments"
