#! /bin/bash
# ASIX-M01 Curs 2018-2019
# Gustavo Tello
# isx43577298
# Descripcio: Processar arguments que són matricules:
# a) llistar les vàlides, del tipus: 9999 AAA.
# b) stdout les que són vàlides, per stderr les no vàlides. Retorna de status el número d̉errors (de no vàlides) 
# Synopsis: prog.sh matricula[...]
cont=0
for matricula in "$@"
do
  echo "$matricula" | egrep "^[0-9]{4} [A-Z]{3}$" 2> /dev/null
  if [ $? -ne 0 ]
  then
   echo "ERROR: $matricula matricula no valida"  >> /dev/stderr	  
   cont=$((cont+1))
  fi
done 
exit $cont




