#! /bin/bash
# @edt ASIX-M01 Curs 2018-2019
# exemples while
# -----------------------------------




# ------- exemples processar stdin --------

# 9) mostrar stdin per stdout numerant 
#    les línies i passant a majúscules
num=1
while read -r line
do
  echo "$num $line" | tr '[a-z]' '[A-Z]'
  num=$((num+1))
done
exit 0

# 8) iterar fins rebre token "FI"
read -r line
while [ $line != "FI"  ]
do
  echo $line
  read -r line
done
exit 0

# 7) ídem numerant línies
num=1
while read -r line
do
  echo "$num: $line"
  num=$((num+1))
done
exit 0

# 6) mostrar línia a línia la entrada
#    estàndard
while read -r line
do
  echo $line
done
exit 0

# ------ exemples de shift i args -------

# 5) ídem numerant arguments
num=1
while [ -n "$1" ]
do
  echo "$num: $1"
  num=$((num+1))
  shift
done
exit 0

# 4) iterar per la llista d'arguments
# mostrant-los un a un. 
# Operador shift.
while [ -n "$1" ]
do
  echo $1
  shift
done
exit 0

# 3) exemple shift
while [ $# -gt 0 ]
do
  echo "$#: $*"
  shift
done
exit 0

# ----  exemples generals while ------

# 2) comptador decramental partint de
# l'argument rebut [n-0]
MIN=0
num=$1
while [ $num -ge $MIN ]
do
  echo -n "$num, "
  num=$((num-1))
done
echo ""
exit 0

# 1) mostrar del [0-10]
MAX=10
num=0
while [ $num -le $MAX ]
do
  echo $num
  num=$((num+1))
done
