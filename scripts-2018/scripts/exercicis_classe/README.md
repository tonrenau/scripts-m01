# @edt ASIX M01-ASO Curs 2018-2019

## UF2 Scripting

Exercicis d'Scripts en Bash

 * Processar arguments
 * Processar l'entrada estàndard
 * Validar arguments
 * Seqüencial
 * Condicional: if/elif/else, case
 * Iterativa: for, while
 * Comptadors, acumuladors, concatenar


#### Exercicis Bàsics

**03-exemple-if.sh** 

  Exemple fe funcionament de l'ordre if.

**04-validar-argument.sh arg1 arg2**

  Validar prog té exactament dos args i els mostra. 
  Si no dos args mostrar error, usage i exit error.
  Explicar que fem primer la validació d’error i pleguem,
   o fem la xixa.

**05-validar-nota.sh  nota**
  
  Mostrar si està aprovat/suspès. Validar rep un arg i que
  la nota és un valor vàlid 0-10.

**06-nota.sh nota**
  
  Mostrar suspès, aprovat, notable, excel·lent. Validar rep
  un arg i que té un valor vàlid del 0 al 10.

**07-esdirectori.sh a dir**

  Si l’argument rebut és un directori, llistar-lo.
  Si no és directori error.

**08-tipusfile.sh  file**

  Dir si es regular, dir, link (ampliar després a altres).
  Usar una variable msg i pintar un sol cop el resultat.
  Ampliació: prog -h, prog file


**09-exemplecase.sh**

- dir si un nom és de nen o de nena donada una llista., - ampliar amb la opció altres
- dir si és vocal o no és vocal
- dir si és parell o imparell del 0 al 10


**10-case-diesmes.sh mes**

  Programa que donat un mes dir si té 28, 30 o 31 dies. 
  L’argument més pren el valor del 1 al 12, cal validar-ho.
  Validar que existeixi l’argument.
  Ampliar fent que tingui un help:  prog -h / prog --help


#### Bucles

**11-exemples-for.sh**

  Exemples d'utilització de for:
 
  * Iterar una llista finita de noms
  * Iterar per els elements de una variable
  * Iterar per els elements que genera una ordre (assignant-la a una variable amb command substitution)
  * iterar per cada argument (usant $*, usant "$*")
  * iterar per a cada argument usant $@, usant "$@"
  * Mostrar la llista d'arguments numerant-los

**12-for-nata.sh nota[...]**

  Processar una o més notes. Validar existeix almenys un argument.
  Validar que cada nota té un valor vàlid del rang [0-10]. 
  Mostrar si suspès, aprovat, notable o excel·lent.
  Tractament de: errors irrecuperables, errors recuperables.
  Tractament d'errors: missatges a stdout usant append ">>".

  Ampliar fent que tingui un help: -h / --help.


**13-exemples-while.sh**

  * mostrar els valors [0-10].
  * comptador decramental partint de l'argument rebut.
  * iterar per la llista d'arguments mostrant-los
    un a un. Operador *shift*. ($1 + shift, usant -n $1).
  * ídem numerant-los.
  * iterar per la llista d'arguments usant ($# -gt 0).
  * iterar línia a línia l'entrada estàndard.
  * ídem fins a rebre un token "FI".
  * convertir les línies rebudes per stdin a majúscules.


**14a-llista-dir.sh dir**

  Validar que es rep un argument i que és un directori i llistar-ne el contingut.
  per llistar el contingut amb un simple ls ja n’hi ha prou.

**14b-llista-dir.sh dir**
  
  Validar que es rep un argument i que és un directori i llistar-ne el contingut numerant cada element (en una llista línia a línia).
  Usar un bucle for i un command substitution per generar la llista d’elements del directori  que cal mostrar-los numeradament.

**14c-llista-dir.sh dir**

  Validar que es rep un argument i que és un directori. Per a cada element del directori indicar si és un fitxer regular, un link, un directori o una altra cosa.
  Usar un bucle per iterar per a cada element del directori (amb un command substitution). Usar un bloc condicional compost per determinar el tipus de l’element.

**14d-llista-dir.sh dir[…]**
  
  Ídem programa anterior però processant un o més directoris.



**15-copyfiles.sh  file… dir-desti**

  Programa que mostri el nº arguments, la llista, el nom del programa,  l'últim i tots menys l’últim. 
  El difícil és identificar quin és l’últim argument. Llavors itera per la llista orfgen i si és file
  el copia al dirdestí.
  Resolt usant grep, sed: “s/.*://”.  
  Copia els fitxers origens al directori destí.
  Validar que el destí és un directori i que els origen són file. Si no és file genera traça d'error.

  * a) Resolt usant grep, sed: “s/.*://”.
  * b) Resolt usant  cut -f$#
  * c) ampliar retornat d'status quans error hi ha hagut.


**16-validarargs.sh**

  * **prog -h (mostrar help)**
  * **prog -a arg (dir primer cas)**
  * **prog -b -a arg  (dir segon cas)**

  Validar arguments i mostrar el missatge que indiqui si estem al primer cas o al segon cas, tot 
  indicant l'argument rebur.

**17-argsfor.sh [ -a -b -c -d -f -g ] arg[…]**

  Mostrar la llista d’opcions i la llista d’arguments.
  Fer un bucle per tots els elements, amb un case acumular 
  les opcions a opcions i els arguments a arguments.
  Les opcions poden aparèixer en qualsevol ordre però 
  sempre abans que els arguments. 
  No cal fer validacions de coherència, suposem que l’usuari
  escriu apropiadament les opcions.
```
  Exemple: prog -a -d -c pau marta pere
  Mostra: 
    opcions: -a -d -c
    args: pau marta pere
```

**18-argsparse.sh  [-a file -b -c -d num -e] arg[…]**

  Mostrar la llista de opcions i la llista d’arguments,
  el valor de file i de num. Fer un bucle per tots els 
  elements, amb un case acumular les opcions a opcions 
  i els arguments a arguments i salvar el valor dels 
  elements file i num. Pista: usar shift.

**19-creardir.sh noudir[…]**

  Crear els directoris passats com arguments. L'ordre mkdir 
  no ha de generar cap sortida. Validar existeix almenys un arg.
  status 0 ok (creats tots els dirs). 1 error args, 2 si algun
  dir no s’ha creat.

