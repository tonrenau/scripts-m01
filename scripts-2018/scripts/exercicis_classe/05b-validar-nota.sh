#! /bin/bash
# @ edt ASIX-M01 Curs 2018-2019
# Validar nota i dor si està suspès
# o aprovat
# -------------------------------
# si num args no es correcte plegar
if [ $# -ne 1 ]
then
  echo "ERROR: num args incorrecte"
  echo "usage: prog nota"
  exit 1
fi
nota=$1
# Validar valor nota
if ! [ $nota -ge 0 -a $nota -le 10 ]
then
  echo "ERROR: valor de nota no vàlid"
  echo "Valors vàlids: 0 - 10"
  exit 2
fi
# Xixa
if [ $nota -lt 5 ]
then
  msg="suspès"
else
  msg="aprovat"
fi
echo $msg
exit 0

