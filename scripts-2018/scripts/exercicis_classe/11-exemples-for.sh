#! /bin/bash
# @edt ASIX-M01 Curs 2018-2019
# Exemples de for
# --------------------------------

# mostrar i numerar els arguments
cont=0
for arg in $*
do
  cont=$((cont+1))
  echo "$cont: $arg"
done
exit 0

# iterar per la llista d'arguments
for arg in "$@"
do
  echo $arg
done
exit 0

# iterar per la llista d'arguments
for arg in $*
do
  echo $arg
done
exit 0

# iterar per els resultats de una ordre
# desada a una var amb command substitution
llista=$(ls)
for nom in $llista
do
  echo $nom
done
exit 0

# iterar el contingut d'una variable
llista="pere pau marta anna"
for nom in $llista
do
  echo $nom
done
exit 0

# Iterar per una llista de noms
for nom in "pere" "pau" "marta" "anna"
do
  echo $nom
done
