#! /bin/bash
# @ edt ASIX-M01 Curs 2018-2019
# $ prog dir
# indicar si dir és o no un directori
# -------------------------------
# si num args no es correcte plegar
ERR_NARGS=1
if [ $# -ne 1 ]
then
  echo "ERROR: num args incorrecte"
  echo "usage: prog dir"
  exit $ERR_NARGS
fi
# Si es demana help
if [ "$1" = "-h" -o  "$1" = "--help" ] 
then
  echo "@edt ASIX-M01"
  echo "usage: prog dir"
  exit 0
fi
# Xixa
dir=$1
if [ -d $dir ]
then
  echo "$dir és un directori"
elif [ -h $dir ]
then 
  echo "$dir és un symbolic link"
elif [ -f $dir ]
then
  echo "$dir és un regular file "
elif ! [ -e $dir ]
then
  echo "$dir: no existeix"
else
  echo "$dir: és una altra cosa"
fi
exit 0

