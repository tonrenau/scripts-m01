#! /bin/bash
# @edt ASIX-M01 Curs 2018-2019
# $prog nota[...]
# mostar si suspès, aprovat, notable, execel·lent
# les una o més notes rebudes.
# --------------------------------
ERR_NARGS=1
OK=0
# si num args no es correcte plegar
if [ $# -lt 1 ]
then
  echo "ERROR: num args incorrecte"
  echo "usage: prog nota"
  exit $ERR_NARGS
fi

llistaNotes=$*
# iterar les notes
for nota in $llistaNotes
do
  # Validar valor nota
  if ! [ $nota -ge 0 -a $nota -le 10 ]
  then
    echo "ERROR: la nota $nota no és vàlida" >> /dev/stderr
    echo "Valors vàlids: 0 - 10" >> /dev/stderr
  elif [ $nota -lt 5 ]
  then
    echo "$nota: suspès"
  elif [ $nota -lt 7 ]
  then
    echo "$nota: aprovat"
  elif [ $nota -lt 9 ]
  then
    echo "$nota: notable"
  else	
    echo "$nota: excel·lent"
  fi
done
exit $OK
