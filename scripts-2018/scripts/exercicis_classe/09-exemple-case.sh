#! /bin/bash
# @edt ASIX-M01 Curs 2018-2019
# exemples case
# -------------------------------
dia=$1
case $dia in
"dl"|"dt"|"dc"|"dj"|"dv")
  echo "dia laborable";;
"ds"|"dm")
  echo "cap de setmana";;
*)
  echo "això no és un dia"
esac
exit 0

lletra=$1
case $lletra in
 'a'|'e'|'i'|'o'|'u')
    echo "$lletra és una vocal";;
 *)
    echo "$lletra és una consonant";;
esac
exit 0
