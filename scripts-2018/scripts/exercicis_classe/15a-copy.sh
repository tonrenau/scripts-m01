#! /bin/bash
# copy.sh file[...] dir-destí
# validar args, dir-destí
# validar file regular file
# ----------------------------
ERR_NARGS=1
ERR_NODIR=2
OK=0
#1) Validar nargs
if [ $# -lt 2 ]; then
  echo "Error: num args incorrecte"
  echo "usage $0 file[...] dir-destí"
  exit $ERR_NARGS
fi
#2) Obtenir elements
dirDesti=$(echo $*| cut -d' ' -f$#)
llistaFile=$(echo $* | cut -d' ' -f1-$(($#-1)))
#3) Validar dir-destí
if ! [ -d $dirDesti ]; then
  echo "Error: $dir no és un dir"
  echo "usage $0 file[...] dir-destí"
  exit $ERR_NODIR
fi
#4) iterar i copiar
for fitxer in $llistaFile
do
  if [ ! -f $fitxer ]; then
    echo "Error: $fitxer not regular" >> /dev/stderr
  else
    cp $fitxer $dirDesti
  fi
done
exit $OK

 
