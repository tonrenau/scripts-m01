#! /bin/bash
# @edt ASIX M01-ISO Curs 2018-2019
# Validar args:
#   prog -h --help
#   prog -a arg
#   prog -b -a arg
# indicar si cas 1 o cas 2 i valor arg
# -----------------------------------------
ERR_NARGS=1
ERR_ARGS=2
OK=0
# Validar n args
if [ $# -lt 1 -o $# -gt 3 ]
then
  echo "ERROR: n args incorrecte"
  echo "usage: $prog -h / -a arg / -b -a arg"
  exit $ERR_NARGS
fi
# Validar ajuda
if [ $# -eq 1 ]
then
  if [ "$1" = "-h" -o "$1" = "--help" ]
  then
    echo "@edt ASIX-M01 Curs 2017-2018"
    echo "usage: $prog -h / -a arg / -b -a arg"
    exit $OK
  else
    echo "ERROR: n args incorrecte"
    echo "usage: $prog -h / -a arg / -b -a arg"
    exit $ERR_ARGS
  fi
fi
# Validar cas1
if [ $# -eq 2 -a "$1" != "-a" ]
then
  echo "Errorformat args"
  echo "usage: $prog -h / -a arg / -b -a arg"
  exit $ERR_ARGS
fi
# Validar cas2
if [ $# -eq 3 -a "$1" != "-b" -a "$2" != "-a" ]
then
  echo "Errorformat args"
  echo "usage: $prog -h / -a arg / -b -a arg"
  exit $ERR_ARGS
fi
# Xixa!
if [ $# -eq 2 ]
then
  echo "Primer cas, arg val $2"
else
  echo "Segon cas, arg val $3"
fi
exit $Ok
