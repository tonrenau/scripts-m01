#! /bin/bash
# @ edt ASIX-M01 Curs 2018-2019
# Validar nota i dir si està suspès
# aprovat notable o excel·lent
# -------------------------------
# si num args no es correcte plegar
if [ $# -ne 1 ]
then
  echo "ERROR: num args incorrecte"
  echo "usage: prog nota"
  exit 1
fi
nota=$1
# Validar valor nota
if ! [ $nota -ge 0 -a $nota -le 10 ]
then
  echo "ERROR: valor de nota no vàlid"
  echo "Valors vàlids: 0 - 10"
  exit 2
fi
# Xixa
if [ $nota -lt 5 ]
then
  echo "suspès"
elif [ $nota -lt 7 ]
then
  echo "aprovat"
elif [ $nota -lt 9 ]
then
  echo "notable"
else	
  echo "excel·lent"
fi
exit 0

