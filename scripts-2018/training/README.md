# @edt ASIX M01-ISO
## Curs 2018-2019

Base de dades training.

usant les taules oficinas, repvantas i pedidos, fer:

 - Normalitzar oficinas a un "," de separador.
 - Normalitzar repventas i pedidos a un espai de separador.
 - Fer la funció showPedidosFromOficina:

Donat un nom d'oficina, llistar per odre de número d'empleat (mostrant 
número d'empleat i nom sencer) les comandes que aquest empleat ha venut
(tabuladament).


