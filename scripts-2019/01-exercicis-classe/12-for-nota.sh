#! /bin/bash
# @edt ASIX-M01 Curs 2019-2020
# gener 2018
# Descripcio: processar nota a nota i 
#   indicar si suspès, aprovat, notable
#   o excel·lent
# prog nota[...]
# ---------------------------------
ERR_NARGS=1
OK=0
# si num args no es correcte plegar
if [ $# -eq 0 ]
then
  echo "ERROR: num args incorrecte"
  echo "usage: $0 nota"
  exit $ERR_NARGS
fi

llistaNotes=$*
for nota in $llistaNotes
do
  if ! [ $nota -ge 0 -a $nota -le 10 ]; then
    echo "ERROR: la nota $nota no és vàlida" >> /dev/stderr
    echo "Valors vàlids: 0 - 10" >> /dev/stderr
  elif [ $nota -lt 5 ];   then
    echo "$nota: suspès"
  elif [ $nota -lt 7 ];   then
    echo "$nota: aprovat"
  elif [ $nota -lt 9 ];   then
    echo "$nota: notable"
  else	
    echo "$nota: excel·lent"
  fi
done
exit 0







