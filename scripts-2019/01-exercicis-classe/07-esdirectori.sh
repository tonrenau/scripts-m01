#! /bin/bash
# @ edt ASIX-M01 Curs 2019-2020
# llistar el directori rebut
# 
# -------------------------------
# si num args no es correcte plegar
if [ $# -ne 1 ]; then
  echo "ERROR: num args incorrecte"
  echo "usage: $0 dir"
  exit 1
fi
# si no és un directori
if ! [ -d $1 ]; then
  echo "ERROR: $1 no és un directori"
  echo "usage: $0 dir"
  exit 2
fi
# xixa
dir=$1
ls $dir
exit 0
