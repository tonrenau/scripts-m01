#! /bin/bash
# @edt ASIX-M01 Curs 2019-2020
# gener 2018
# Descripcio: exemples bucle for
# ---------------------------------

# mostrar i numerar llista arguments
nlin=0
for arg in $*
do
  nlin=$((nlin+1))
  echo "$nlin: $arg"
done
exit 0

# iterar per la llista d'arguments
for arg in "$@"
do
  echo $arg
done
exit 0

# iterar per la llista d'arguments
for arg in $*
do
  echo $arg
done
exit 0

# iterar per un contingut d'una variable
llistat=$(ls)
for nom in $llistat
do
  echo $nom
done
exit 0

# iterar per el contingut d'una cadena
# només itera un cop
for nom in "pere pau marta anna"
do
  echo $nom
done
exit 0

# iterar per un conjunt d'elements
for nom in "pere" "pau" "marta" "anna"
do
  echo $nom
done
exit 0
