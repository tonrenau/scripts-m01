#! /bin/bash
# @edt ASIX M01-ASO Curs 2019-2020
# $ llistar-dir.sh dir
# fa un 'ls' del directori rebut
# verificar 1 arg, i que és un dir
# -------------------------------------
ERR_NARGS=1
ERR_NODIR=2
# 1) validar arguments
if [ $# -eq 0 ]
then
  echo "Error: número args no vàlid"
  echo "usarge: $0 dir[...]"
  exit $ERR_NARGS
fi

for dir in $*
do 
  # 2) validar arg és un dir
  if ! [ -d $dir ]
  then
    echo "Error: $dir no és un directori" >> /dev/stderr
    echo "usarge: $0 dir"  >> /dev/stderr
  else
    # 3) xixa
    echo "Processant directori: $dir"
    cont=1
    fileList=$(ls $dir)
    for elem in $fileList
    do
      if [ -h $dir/$elem ]
      then
        echo -e "\t$cont $elem link"
      elif [ -d $dir/$elem ]
      then
        echo -e "\t$cont $elem dir"
      elif [ -f $dir/$elem ]
      then
        echo -e "\t$cont $elem regular"
      else
        echo -e "\t$cont $elem altres"
      fi 
      cont=$((cont+1))
    done
  fi
done
exit 0



