#! /bin/bash
# @ edt ASIX-M01 Curs 2019-2020
# Validar nota: suspes, aprovat
# notable, excel·lent
# -------------------------------
# si num args no es correcte plegar
if [ $# -ne 1 ]; then
  echo "ERROR: num args incorrecte"
  echo "usage: $0 nota"
  exit 1
fi
# si nota no és [0-10] plegar
if ! [ $1 -ge 0 -a $1 -le 10 ]; then
  echo "ERROR: nota $1 novalida [0-10]"
  echo "usage: $0 nota"
  exit 2
fi
# xixa
nota=$1
if [ $nota -lt 5 ]; then
  echo "Suspès"	
elif [ $nota -lt 7 ]; then
  echo "Aprovat"
elif [ $nota -lt 9 ]; then
  echo "Notable"
else
  echo "Excel·lent"
fi
exit 0

