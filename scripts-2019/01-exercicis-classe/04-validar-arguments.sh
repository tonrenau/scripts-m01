#! /bin/bash
# @ edt ASIX-M01 Curs 2019-2020
# Validar que té exàctament 2 args
# i mostarr-los
# -------------------------------
# si num args no es correcte plegar
if [ $# -ne 2 ]
then
  echo "ERROR: num args incorrecte"
  echo "usage: prog arg1 arg2"
  exit 1
fi
# xixa
echo "Els dos args son: $1, $2 "
exit 0

