#! /bin/bash 
#Diego sanchez
#asix m01
#programa que crea un grup d'usuaris donat el nom del curs

ERR_NARGS=1
OK=0
#comprovar que hi ha 2 args
if [ $# -ne 2 ]; then
  echo "error: nº d'args $# incorrecte" >> /dev/stderr
  echo "usage: $0 curs file.txt"
  exit $ERR_NARGS
fi

#crear el home del curs dels alumnes
curs=$1
groupadd $curs
mkdir /home/$curs

#crear el usuaris
file=$2
while read -r line; do
  user=$(echo $line | cut -d: -f1)
  password=$(echo $line | cut -d: -f2)
  useradd -g $curs -G users -m -b /home/$curs $user
  echo $line | chpasswd 
done < $file

exit $OK
