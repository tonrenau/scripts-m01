#! /bin/bash
#edt @asix m01
#diego sanchez piedra
#-----------------------
#28) showShadowIn < login
function showShadowIn(){
  ERR_LOGIN=1
  status=0
  while read -r login; do
    egrep "^$login:" /etc/passwd &> /dev/null
    if [ $? -ne 0 ]; then
      echo "error: el login $login no existeix" >> /dev/stderr
      ((status++))
    else
      showShadow $login
    fi
  done
}
#------------------------
#27) showShadowList
function showShadowList(){
  ERR_NARGS=1
  status=0
  #validar el nº d'args
  if [ $# -lt 1 ]; then
    echo "error: nº d'args $# incorrecte" >> /dev/stderr
    echo "usage: showShadowList login[...]" >> /dev/stderr
  fi

  #per cada login...
  loginList=$*
  for login in $loginList; do
    #validar que el login existeix
    egrep "^$login:" /etc/passwd &> /dev/null
    if [ $? -ne 0 ]; then
      echo "login $login no existeix" >> /dev/stderr
      ((status++))
    else
      showShadow $login
    fi
  done
  return $status
}
#-------------------------
#26) showShadow login
function showShadow(){
  ERR_NARGS=1
  ERR_LOGIN=2
  OK=0
  #comprovar que es rep un arg
  if [ $# -ne 1 ]; then
    echo "error: nº d'args incorrecte" >> /dev/stderr
    echo "usage: showShadow login" >> /dev/stderr
    return $ERR_NARGS
  fi

  #comprovar que el login existeix
  login=$1
  egrep "^$login:" /etc/passwd &> /dev/null
  if [ $? -ne 0 ]; then
    echo "error: login $login no existeix"
    return $ERR_LOGIN
  fi
  #xixa
  shadowLine=$(egrep "^$login:" /etc/shadow)
  encryptedPass=$(echo $shadowLine | cut -d: -f2)
  dateOfLastPassChange=$(echo $shadowLine | cut -d: -f3)
  minPassAge=$(echo $shadowLine | cut -d: -f4)
  maxPassAge=$(echo $shadowLine | cut -d: -f5)
  passWarnPeriod=$(echo $shadowLine | cut -d: -f6)
  passInactivityPeriod=$(echo $shadowLine | cut -d: -f7)
  AccountExpirationDate=$(echo $shadowLine | cut -d: -f8)
  reservedField=$(echo $shadowLine | cut -d: -f9)
  
  echo "login: $login"
  echo "encryptedPass: $encryptedPass"
  echo "dateOfLastPassChange: $dateOfLastPassChange"
  echo "minPassAge: $minPassAge"
  echo "maxPassAge: $maxPassAge"
  echo "passWarnPeriod: $passWarnPeriod"
  echo "passInactivityPeriod: $passInactivityPeriod"
  echo "AccountExpirationDate: $AccountExpirationDate"
  echo "reservedField: $reservedField"
  return $OK
}
