#! /bin/bash 
#Diego sanchez
#asix m01
#programa que crea un grup d'usuaris donat el nom del curs

ERR_NARGS=1
OK=0
#comprovar que ,com a minim hi ha, 2 args
if [ $# -lt 2 ]; then
  echo "error: nº d'args $# incorrecte" >> /dev/stderr
  echo "usage: $0 curs login"
  exit $ERR_NARGS
fi

#crear el home del curs dels alumnes
curs=$1
groupadd $curs &> /dev/null
mkdir /home/$curs &> /dev/null

shift
#crear el usuaris
userList=$*

for user in $userList; do
  #generar el password aleatori
  pw=$(cat /dev/urandom | tr -dc 'a-zA-Z0-9' | fold -w 8 | head -n1)
  useradd -g $curs -G users -m -b /home/$curs $user
  echo "$user:$pw" | chpasswd
  echo "$user:$pw" >> passwd.log
done
exit $OK
