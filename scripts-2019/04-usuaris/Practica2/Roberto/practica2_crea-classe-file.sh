#! /bin/bash
# Roberto Martínez
# 08/04/2020
# Programa que crea alumnes del curs que rep com a primer argument. Rep
# un segon argument corresponent a un fitxer amb noms:password dels 
# alumnes a donar de alta. 
# ------------------------------------------------------------------
status=0
ERR_NARGS=1
HOME_BASE=/home/inf
SECONDARY_GROUP=users
# 1) validar arguments
if [ $# -ne 2 ]; then
  echo "Error: num args incorrecte"
  echo "Usage: $0 classe fitxer_alumnes"
  exit $ERR_NARGS
fi

# 2) crear grup si cal
group=$1
egrep "^$group:" /etc/group &> /dev/null
if [ $? -ne 0 ]; then
  groupadd $group && echo "Grup $group creat"
else
  echo "Grup $group ja existeix al sistema"
  status=2
fi
if [ $? -ne 0 ]; then
  groupadd $SECONDARY_GROUP && echo "Grup $SECONDARY_GROUP creat"
else
  echo "Grup $SECONDARY_GROUP ja existeix al sistema"
  status=3
fi

# 3) crear directori base si cal
mkdir -p $HOME_BASE/$group &> /dev/null && echo "Directori base $HOME_BASE/$group creat"
if [ $? -ne 0 ]; then
  echo "Directori base $HOME_BASE/$group ja existeix al sistema" >> /dev/stderr
  status=4
fi
chgrp $group $HOME_BASE/$group && echo "Directori base $HOME_BASE/$group canviat a grup $group correctament"

# 4) crear usuaris
llistaUsers=$(cat $2)
for line in $llistaUsers
do
  user=$(echo $line | cut -d: -f1)
  password=$(echo $line | cut -d: -f2)
  useradd -m -n -g $group -G $SECONDARY_GROUP -b $HOME_BASE/$group -p $password $user &> /dev/null && echo "Usuari $user creat"
  if [ $? -ne 0 ]; then
    echo "Usuari $user ja existeix al sistema" >> /dev/stderr
    status=5
  fi
done
exit $status

