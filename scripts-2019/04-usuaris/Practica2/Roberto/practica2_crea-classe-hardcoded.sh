#! /bin/bash
# Roberto Martínez
# 08/04/2020
# Programa que crea usuaris per a 30 alumnes. Tots els usuaris
# tenen hisx1 com a grup principal (si no existeix es crea) i el seu
# home ha de pertànyer a la ruta /home/inf/hisx1.
# ------------------------------------------------------------------
HOME_BASE=/home/inf
GROUP=hisx1
SECONDARY_GROUP=users
status=0
# 1) crear grups si calen
egrep "^$GROUP:" /etc/group &> /dev/null
if [ $? -ne 0 ]; then
  groupadd $GROUP && echo "Grup $GROUP creat"
else
  echo "Grup $GROUP ja existeix al sistema"
  status=1
fi
egrep "^$SECONDARY_GROUP:" /etc/group &> /dev/null
if [ $? -ne 0 ]; then
  groupadd $SECONDARY_GROUP && echo "Grup $SECONDARY_GROUP creat"
else
  echo "Grup $SECONDARY_GROUP ja existeix al sistema"
  status=2
fi

# 2) crear directori base si cal
mkdir -p $HOME_BASE/$GROUP &> /dev/null && echo "Directori base $HOME_BASE/$GROUP creat"
if [ $? -ne 0 ]; then
  echo "Directori base $HOME_BASE/$GROUP ja existeix al sistema" >> /dev/stderr
  status=3
fi
chgrp $GROUP $HOME_BASE/$GROUP && echo "Directori base $HOME_BASE/$GROUP canviat a grup $GROUP correctament"

# 3) crear usuaris
llistaUsers=$(echo $GROUP-{01..30})
for user in $llistaUsers
do
  useradd -m -n -g $GROUP -G $SECONDARY_GROUP -b $HOME_BASE/$GROUP -p "alum" $user &> /dev/null && echo "Usuari $user creat"
  if [ $? -ne 0 ]; then
    echo "Usuari $user ja existeix al sistema" >> /dev/stderr
    status=4
  fi
done
exit $status

