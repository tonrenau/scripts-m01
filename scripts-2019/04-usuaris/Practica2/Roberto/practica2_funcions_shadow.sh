#! /bin/bash
# Roberto Martínez
# 03/04/2020
# Funcions shadow
# -----------------------------------
# 26) Donat un login mostra els camps de /etc/shadow
# mostra camp a camp amb label i el seu valor.
# Valida que es rep argument i que login existeix.

function showShadow(){
  OK=0
  ERR_NARGS=1
  ERR_NOLOGIN=2
  if [ $# -ne 1 ]; then
    echo "Error: num args"
    echo "Usage: function login"
    return $ERR_NARGS
  fi
  login=$1
  line=""
  line=$(grep "^$login:" /etc/shadow 2> /dev/null)
  if [ -z "$line"]; then
    echo "Error: login $login no existeix al sistema"
    echo "Usage: function login"
    return $ERR_NOLOGIN
  fi
  camp2=$(echo $line | cut -d: -f2)
  camp3=$(echo $line | cut -d: -f3)
  camp4=$(echo $line | cut -d: -f4)
  camp5=$(echo $line | cut -d: -f5)
  camp6=$(echo $line | cut -d: -f6)
  camp7=$(echo $line | cut -d: -f7)
  echo "Login: $login"
  echo "Password: $camp2"
  echo "Date of last password change: $camp3"
  echo "Minimun password age: $camp4"
  echo "Maximun password age $camp5"
  echo "Password warning period: $camp6"
  echo "Account expiration date: $camp7"
  return $OK
}


# 27) Mostra per a cada login les dades del seu shadow.
# Validar argument i que login existeix.

function showShadowList(){
  status=0
  ERR_NARGS=1
  ERR_NOLOGIN=2
  if [ $# -lt 1 ]; then
    echo "Error: num args"
    echo "Usage: function login[...]"
    return $ERR_NARGS
  fi
  loginList=$*
  for login in $loginList
  do
    line=""
    line=$(grep "^$login:" /etc/shadow 2> /dev/null)
    if [ -z "$line"]; then
      echo "Error: login $login no existeix al sistema" >> /dev/stderr
      echo "Usage: function login[...]" >> /dev/stderr
      status=$ERR_NOLOGIN
    else
      camp2=$(echo $line | cut -d: -f2)
      camp3=$(echo $line | cut -d: -f3)
      camp4=$(echo $line | cut -d: -f4)
      camp5=$(echo $line | cut -d: -f5)
      camp6=$(echo $line | cut -d: -f6)
      camp7=$(echo $line | cut -d: -f7)
      echo "Login: $login"
      echo "Password: $camp2"
      echo "Date of last password change: $camp3"
      echo "Minimun password age: $camp4"
      echo "Maximun password age $camp5"
      echo "Password warning period: $camp6"
      echo "Account expiration date: $camp7"
    fi
  done
  return $status
}


# 28) Mostra camps del shadow de cada usuari rebut per stdin.
# Cada línia de entrada stàndard correspon a un login.
# Verifica que login existeix.

function showShadowIn(){
  status=0
  ERR_NOLOGIN=1
  while read -r login
  do
    line=""
    line=$(grep "^$login:" /etc/shadow 2> /dev/null)
    if [ -z "$line"]; then
      echo "Error: login $login no existeix al sistema" >> /dev/stderr
      echo "Usage: function < login " >> /dev/stderr
      status=$ERR_NOLOGIN
    else
      camp2=$(echo $line | cut -d: -f2)
      camp3=$(echo $line | cut -d: -f3)
      camp4=$(echo $line | cut -d: -f4)
      camp5=$(echo $line | cut -d: -f5)
      camp6=$(echo $line | cut -d: -f6)
      camp7=$(echo $line | cut -d: -f7)
      echo "Login: $login"
      echo "Password: $camp2"
      echo "Date of last password change: $camp3"
      echo "Minimun password age: $camp4"
      echo "Maximun password age $camp5"
      echo "Password warning period: $camp6"
      echo "Account expiration date: $camp7"
    fi
  done
  return $status
}
