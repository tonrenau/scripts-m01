#! /bin/bash

# 5)--------------------------------------
# filterGidDefault [file]
# Mostrar els usuaris de gid >= 500
# Es rep un file amb liies tipus /etc/passwd
# Validar rep un arg i validar és regular
# file
# Si no es rep el file es processa stdin
# --------------------------------------
function filterGid(){
  ERR_NARGS=1 
  ERR_NOFILE=2
  #1) validar arg
  if [ $# -gt 1 ]; then
    echo "Error: num args incorrecte"
    echo "usage: $0 file"
    return $ERR_NARGS
  fi
  #2) validar un arg i arg es regular file
  fileIn=/dev/stdin
  if [ $# -eq 1 ]; then 
    if [ ! -f "$1" ]; then
      echo "Error: $1 no és un fitxer"
      echo "usage: $0 file"
      return $ERR_NOFILE
    fi
    fileIn=$1
  fi
  while read -r line
  do
    gid=$(echo $line | cut -d: -f4)
    if [ $gid -ge 500 ]; then
      echo $line | cut -d: -f1,3,4,6
    fi
  done < $fileIn
  return 0  
}

# 4)--------------------------------------
# filterGid(file)
# Mostrar els usuaris de gid >= 500
# Es rep un file amb liies tipus /etc/passwd
# Validar rep un arg i validar és regular
# file
# --------------------------------------
function filterGid(){
  ERR_NARGS=1 
  ERR_NOFILE=2
  #1) validar arg
  if [ $# -ne 1 ]; then
    echo "Error: num args incorrecte"
    echo "usage: $0 file"
    return $ERR_NARGS
  fi
 #2) validar arg es regular file
  if [ ! -f "$1" ]; then
    echo "Error: $1 no és un fitxer"
    echo "usage: $0 file"
    return $ERR_NOFILE
  fi
  fileIn=$1 
  while read -r line
  do
    gid=$(echo $line | cut -d: -f4)
    if [ $gid -ge 500 ]; then
      echo $line | cut -d: -f1,3,4,6
    fi
  done < $fileIn
  return 0
}

# 3)--------------------------------------
# numFile [file]
# Mostrar linia a línia file numerat
# processa file o stdin
# si rep file validar és regular file
# --------------------------------------
function numFileDefault(){
  ERR_NARGS=1 
  ERR_NOFILE=2
  #1) validar arg
  if [ $# -gt 1 ]; then
    echo "Error: num args incorrecte"
    echo "usage: $0 file"
    return $ERR_NARGS
  fi
  #2) validar un arg i arg es regular file
  fileIn=/dev/stdin
  if [ $# -eq 1 ]; then 
    if [ ! -f "$1" ]; then
      echo "Error: $1 no és un fitxer"
      echo "usage: $0 file"
      return $ERR_NOFILE
    fi
    fileIn=$1
  fi
  cont=1
  while read -r line
  do
    echo "$cont: $line"
    ((cont++))
  done < $fileIn
}

# 2)--------------------------------------
# numFile(file)
# Mostrar linia a línia file numerat
# Validar rep un arg i validar és regular
# file
# --------------------------------------
function numFile(){
  ERR_NARGS=1 
  ERR_NOFILE=2
  #1) validar arg
  if [ $# -ne 1 ]; then
    echo "Error: num args incorrecte"
    echo "usage: $0 file"
    return $ERR_NARGS
  fi
 #2) validar arg es regular file
  if [ ! -f "$1" ]; then
    echo "Error: $1 no és un fitxer"
    echo "usage: $0 file"
    return $ERR_NOFILE
  fi
  fileIn=$1 
  cont=1
  while read -r line
  do
    echo "$cont: $line"
    ((cont++))
  done < $fileIn
}


# 1)--------------------------------------
# numStdin
# Mostrar linia a línia stdin numerat
# --------------------------------------
function numStdin(){
  cont=1
  while read -r line
  do
    echo "$cont: $line"
    ((cont++))
  done	
}


