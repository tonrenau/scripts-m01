#! /bin/bash
#Programa 1 processa arguments
#curso 2018 2019
#2) Processar els arguments i comptar quantes n̉hi ha de 3 o més caràcters
#-----------------------------------------
num=0
for args in $*
do
	echo $args | egrep '.{3}' &> /dev/null
	if [ $? -eq 0 ]
	then 
		num=$((num+1))
	fi
done
echo "hi ha $num de 3 o més caràcters"
exit 0

