#! /bin/bash
# @edt ASIX-M01 Curs 2018-2019
# Adrián Narváez isx39448945
# Descripcio: Processar stdin cmostrant per stdout les línies numerades i en majúscules.
# Synopsis: prog.sh 
OK=0
cont=0
while read -r line
do
  cont=$((cont+1))
  echo $cont $line | tr '[:lower:]' '[:upper:]'
done
exit $OK




