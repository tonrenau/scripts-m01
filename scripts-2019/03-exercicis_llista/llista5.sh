#! /bin/bash
# @edt ASIX-M01 Curs 2018-2019
# Adrián Narváez isx39448945
# Descripcio: Processar stdin mostrant per stdout les línies de menys de 50 caràcters.
# Synopsis: prog.sh
OK=0
while read -r line
do
  chars=$(echo $line | wc -c)
  if [ $chars -lt 50 ]
  then
    echo $line
  fi
done
exit $OK

