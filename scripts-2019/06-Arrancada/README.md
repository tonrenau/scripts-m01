# @edt ASIX M01-ISO
## Curs 2019-2020

Podeu trobar els programes al GitLab de [edtasixm01](https://gitlab.com/edtasixm01/scripts-2019.git
)

Podeu trobar la documentació del mòdul a [ASIX-M01](https://sites.google.com/site/asixm01edt/)

ASIX M06-ASO Escola del treball de barcelona


### Arrancada

En aquest bloc treballarem tres aspectes de l'arrencada del sistema:

 * 01 - Arrancada amb Systemd
 * 02 - Arrancada amb Grub
 * 03 - Arrancada / Instal·lacions

Per poder treballar aquests aspectes, en especial Grub i Instal3lacions és convenient
preparra un petit laboratori de treball, una Màquina Virtual on fer les pràctiques. Els
passos, explicacions i vídeos per treballar amb VM són a:

 * 00 -  Preparació Lab


