1) iterar per la llista d’arguments i fer una “acció”,
 mostrar visca si l’acció a anat bé i kaka si ha fallat.

for arg in $*
do
  acció
  if [ $? -eq 0 ]; then
    echo "visca"
  else
    echo "kaka"
  fi
done

4) Iterar la llista de logins, si existeix stdout, si no existeix
stderr, mostrar per pantalla el total, els oks i els errors. 
Retorna 0 si tots oks i 1 si hi algun error.
oks=0
err=0
for login in $*
do
  grep -q "^$login:" /etc/passwd
  if [ $? -eq 0 ]; then
    echo "$login"
    ((oks++))
  else
    echo "$login" >> /dev/stderr
    ((err++))
  fi
done
echo "$# $oks $err"
if [ $err -ne 0 ]; then
  exit 1
fi
exit 0  

3) Programa grup login…
usant usermod -G group user, afegir cada usuari al grup indicat.
Si es produeix error mostrar missatge d’error per stderr.
Retorna el número d’errors.

# prog 1hisix pere marta anna
err=0
grup=$1
shift
for login in $*
do
  usermod -G $grup $login
  if [ $? -ne 0 ]; then
    echo "$grup $login no associat" 1>&2
    ((err++))
  fi  
done
exit $err

4) Es disposa del programa primer que rep un número i retorna 0
 si és primer, 1 si és no-primer imparell i 2 si és no-primer parell.
 
Processar l’entrada estàndard, es reben números i si és primer 
es mostra el número per stdout i si no ho és es mostra per stderr. 
Retorna el número total de no primers.


















































