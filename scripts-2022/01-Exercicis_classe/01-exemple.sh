#! /bin/bash
# @edt ASIX-M01
# Febrer 2023
#
# Exemple de primer programa
# Normes:
#   shebang (#!) indica qui ha d'interpretar el fitxer
#   caçalera: descripció, data, autor
# -----------------------------------------------

echo "hola mon"
nom="pere pou prat"
edat=25
echo $nom $edat
echo -e "nom: $nom\n edat: $edat\n"
echo -e 'nom: $nom\n edat: $edat\n'
uname -a
uptime
#ps
echo $SHELL
echo $SHLVL
echo $((4*32))
echo $((edat*2))

#read data1 data2
#echo -e "$data1 \n $data2"
exit 0
