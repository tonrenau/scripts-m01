# @jordijsmx ASIX-M01
# Març 2023
#
# Rep una o més notes d'un alumne i indica, per cada nota rebuda
# si és un suspès, aprovat, notable o excel·lent.
# ------------------------
ERR_NARGS=1
ERR_NOTA=2
# Validar arguments
if [ $# -eq 0 ];then
  echo "Error: número d'arguments incorrecte."
  echo "Usage: $0 nota..."
  exit $ERR_NARGS
fi

# Validar que els arguments són un nombre [0-10]
# i codi
for nota in $*
do
  if ! [ $nota -ge 0 -a $nota -le 10 ];then
    echo "Error: $nota no entre [0-10]" >> /dev/stderr
  elif [ $nota -lt 5 ];then
    echo "$num: Has tret un $nota. Suspès."
  elif [ $nota -lt 7 ];then
    echo "$num: Has tret un $nota. Aprovat."
  elif [ $nota -lt 9 ];then
    echo "$num: Has tret un $nota. Notable."
  else
    echo "$num: Has tret un $nota. Excel·lent."
  fi
done
exit 0
	

