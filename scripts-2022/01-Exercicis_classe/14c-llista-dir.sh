#! /bin/bash
# @edt ASIX M01-ASO Curs 2022-2023
# Març 2023
# $ llistar-dir.sh dir
# ampliat indicant per cada element si és link, dir, 
# regular o altra cosa
# -------------------------------------
ERR_NARGS=1
ERR_NODIR=2
# 1) validar arguments
if [ $# -ne 1 ]
then
  echo "Error: número args no vàlid"
  echo "usage: $0 dir"
  exit $ERR_NARGS
fi
dir=$1
# 2) validar arg és un dir
if ! [ -d $dir ]
then
  echo "Error: $dir no és un directori"
  echo "usage: $0 dir"
  exit $ERR_NODIR
fi
# 3) xixa
llista=$(ls $dir)
for elem in $llista
do
  if [ -h "$dir/$elem" ]; then
    echo "$elem és un link"
  elif [ -d "$dir/$elem"  ]; then
    echo "$elem és un dir"
  elif [ -f "$dir/$elem" ]; then
    echo "$elem és un regular"      	  
  else
    echo "$elem és una altra cosa"
  fi  
done
exit 0



