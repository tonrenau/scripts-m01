! /bin/bash
# @ edt ASIX-M01 Curs 2022-2023
# $ prog file
# indicar si dir és: regular, dir o link
# --------------------------------------
# si num args no es correcte plegar
ERR_NARGS=1
ERR_NOEXIST=2
if [ $# -ne 1 ]
then
  echo "ERROR: num args incorrecte"
  echo "usage: $0 fit"
  exit $ERR_NARGS
fi
# Xixa
fit=$1
if [ ! -e $fit  ]; then
  echo "$fit no existeix"
  exit $ERR_NOEXIST  
elif [ -f $fit ]; then
  echo "$fit és un regular file"
elif [ -h $fit ]; then
  echo "$fit és un link"
elif [ -d $fit ]; then
  echo "$fit és un directori"
else
  echo "$fit és una altra cosa"	
fi
exit 0

