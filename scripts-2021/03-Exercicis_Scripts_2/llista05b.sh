#! /bin/bash
#EDT Mati Vizcaino
#Programa 1 processa arguments
#curso 2018 2019
#5) Processar stdin mostrant per stdout les línies de menys de 50 caràcters.
#Programa 2 processa stdin
while read -r line
do
	echo "$line" | egrep '^.{,50}$'
done
exit 0
