#! /bin/bash
# @edt ASIX M01-ISO Curs 2021-2022
# 
# prog -a file -b -c -d num -f arg[...]
# -------------------------------------
opcions=""
arguments=""
fitxer=""
num=""
while [ "$1" ]
do
  case "$1" in
   "-b"|"-c"|"-e")
     opcions="$opcions $1";;
   "-a")
     opcions="$opcions $1"
     fitxer=$2
     shift;;
   "-d")
     opcions="$opcions $1"
     num=$2
     shift;; 
   *)
     arguments="$arguments $1";;	  
  esac	  
  shift  
done
echo "Opcions: $opcions"
echo "Arguments: $arguments"
echo "Fitxer: $fitxer"
echo "Num: $num"
exit 0
