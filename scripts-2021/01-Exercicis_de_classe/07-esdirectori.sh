#! /bin/bash
# @ edt ASIX-M01 Curs 2021-2022
#
# llistar el directori rebut
# -------------------------------
ERR_ARGS=1
ERR_NODIR=2
# si num args no es correcte plegar
if [ $# -ne 1 ]; then
  echo "ERROR: num args incorrecte"
  echo "usage: $0 dir"
  exit $ERR_ARGS
fi
# si no és un directori plegar
if [ ! -d $1  ]; then
  echo "ERROR: $1 no és un directori"
  echo "usage: $0 dir"
  exit $ERR_NODIR
fi	
# xixa
dir=$1
ls $dir
exit 0








