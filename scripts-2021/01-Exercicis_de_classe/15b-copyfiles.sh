#! /bin/bash
# @edt Curs 2021-2022
# copy.sh file...  dir-destí
# validar dir-destí és dir i existeix
# validar file... són regular files
# ----------------------------
ERR_NARGS=1
ERR_NODIR=2
ERR_NOREGULARFILE=3
OK=0
#1) Validar nargs
if [ $# -lt 2 ]; then
  echo "Error: num args incorrecte"
  echo "usage $0 file[...] dir-destí"
  exit $ERR_NARGS
fi
# 2) separar destí i llista_files
#llista_files="carta.txt treball.txt noexist.pwd" 
#desti="/tmp"
#desti=$(echo $* | sed 's/^.* //')
#llista_file=$(echo $* | sed 's/ [^ ]*$//'
desti=$(echo $* | cut -d' ' -f$#)
llista_files=$(echo $* | cut -d' ' -f1-$(($#-1)) )
echo $desti
echo $llista_files
exit 0

# 3) dir-desti existeix
if [ ! -d $desti ]; then
  echo "Error: $desti no és un directori"
  echo "usage $0 file dir-destí"
  exit $ERR_NODIR
fi

# 3) Iterar file a file
for file in $llista_files
do	
  if [ ! -f $file ]; then
    echo "Error: $file no és un regular file" >&2
  else
    cp $file $desti
  fi
done
exit 0

