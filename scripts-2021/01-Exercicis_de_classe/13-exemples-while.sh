#! /bin/bash 
# @edt ASIX-M01 Curs 2021-2022
# febrer 2022
# Descripcio: exemples bucle while 
# ---------------------------------

# 7) numerar stdin linia a línia i passar a 
#    majúscules
num=1
while read -r line
do
  echo "$num: $line " | tr 'a-z' 'A-Z'
  ((num++))
done
exit 0


# 6) processar stdin fins al token FI
read -r line
while [ "$line" != "FI" ]
do
  echo "$line"
  read -r line  
done	
exit 0

# 5) numerar stdin línia  línia
num=1
while read -r line
do      
  echo "$num: $line "
  ((num++))
done
exit 0


# 4) Processar l'entrada estandard
#    línia a línia   
while read -r line
do
  echo $line	
done	
exit 0

# 3) iterar arguments amb shift
while [ -n "$1" ]
do
  echo "$1, $#, $*"
  shift  
done
exit 0

# 2) comptador decreixent del arg [N-0]
MIN=0
num=$1
while [ $num -ge $MIN ] 
do
  echo -n "$num, "
  ((num--)) 
done	
exit 0

# 1) mostrar un comptador del 1 a MAX
MAX=10
num=1
while [ $num -le $MAX ] 
do
  echo "$num"
  ((num++))
done	
exit 0

