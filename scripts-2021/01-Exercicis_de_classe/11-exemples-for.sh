#! /bin/bash
# @edt ASIX-M01 Curs 2021-2022
# Febrer 2022
# Descripcio: exemples bucle for
# ---------------------------------

# 8) Llistar les logins numerats
llista_noms=$(cut -d: -f1 /etc/passwd | sort)
num=1
for elem in $llista_noms
do
  echo "$num: $elem"
  ((num++))
done
exit 0


# 7) Llistar numerat els noms dels files 
#    del directori actiu
llista_noms=$(ls)
num=1
for elem in $llista_noms
do
  echo "$num: $elem"
  ((num++))
done
exit 0


# 6) llistar els arguments numerats
num=1
for arg in $*
do
  echo "$num: $arg"
  num=$(($num+1))
done
exit 0


# 5) iterar per la llista d'arguments
# $@ expantendeix els words encata que 
# estiguin encapsulats
for arg in "$@"
do
  echo $arg     
done
exit 0

# 4) iterar per la llista d'arguments
for arg in $*
do
  echo $arg	
done	
exit 0


# 3) iterar pel valor d'una variable
llistat=$(ls)
for nom in $llistat
do
  echo $nom	
done
exit 0


# 2) iterar per un conjunt d'elements
for nom in "pere pau marta anna"
do
  echo "$nom"
done
exit 0


# 1) iterar per un conjunt d'elements
for nom in "pere" "pau" "marta" "anna"
do
  echo "$nom"
done
exit 0
